USE resto;

CREATE TABLE `recette` (
  `id_recette` INT PRIMARY KEY AUTO_INCREMENT,
  `nom_recette` VARCHAR(255) UNIQUE
);

CREATE TABLE `produit_recette` (
  `id_produit` INT,
  `id_recette` INT,
  `quantite` INT,
  FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id_produit`),
  FOREIGN KEY (`id_recette`) REFERENCES `recette` (`id_recette`),
  PRIMARY KEY (`id_produit`, `id_recette`)
);
