import pandas as pd
import mysql.connector
from pprint import pprint
from unidecode import unidecode

def normalize_text(text):
    text = unidecode(text)

    first_space_index = text.find(' ')
    if first_space_index != -1:
        text = text[:first_space_index].capitalize() + text[first_space_index:]
    else:
        text = text.capitalize()

    if text.endswith('s') and text.lower() not in ['parfums']:
        text = text[:-1]
    return text


file_path = 'extract_B.csv'
data = pd.read_csv(file_path)
data['stock'] = data['stock'].apply(normalize_text)
data['date'] = pd.to_datetime(data['date'], format='%d/%m/%Y')
data.rename(columns={'stock': 'produit', 'inventory': 'quantite'}, inplace=True)
data = data[['produit', 'quantite', 'date']]
grouped_data = data.loc[data.groupby('produit')['date'].idxmax()].reset_index(drop=True)
# pprint(grouped_data)

mydb = mysql.connector.connect(
    host='localhost',
    user='alucard',
    password='coucou123',
    db='resto'
)
cursor = mydb.cursor()

nom_restaurant = "Restaurant_B"
id_restaurant = 2
cursor.execute("INSERT INTO restaurant (id_restaurant, nom_restaurant) VALUES (%s, %s) ON DUPLICATE KEY UPDATE nom_restaurant = VALUES(nom_restaurant)", (id_restaurant, nom_restaurant))
mydb.commit()

for index, row in grouped_data.iterrows():
    try:
        cursor.execute("SELECT id_produit FROM produit WHERE nom_produit = %s", (row['produit'],))
        result = cursor.fetchone()
        cursor.fetchall()

        if not result:
            cursor.execute("INSERT INTO produit (nom_produit) VALUES (%s)", (row['produit'],))
            mydb.commit()
            id_produit = cursor.lastrowid
        else:
            id_produit = result[0]

        cursor.execute("""
            INSERT INTO stock (quantite, date_inventaire, id_produit, id_restaurant) 
            VALUES (%s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE 
            quantite = quantite + VALUES(quantite), 
            date_inventaire = VALUES(date_inventaire)
            """, (row['quantite'], row['date'].strftime('%Y-%m-%d'), id_produit, id_restaurant))
        mydb.commit()
    except mysql.connector.Error as err:
        print("Une erreur est survenue:", err)

cursor.close()
mydb.close()