-- la ibiza crepe

-- 1 oeuf
-- 100g de farine
-- 100g de sucre
-- 100ml de lait
-- 1g mdma

USE resto;

INSERT INTO recette (nom_recette) VALUES ('La Ibiza Crêpe');

SELECT id_recette FROM recette WHERE nom_recette = 'La Ibiza Crêpe';

SET @id_recette = (SELECT id_recette FROM recette WHERE nom_recette = 'La Ibiza Crêpe');

INSERT INTO produit (nom_produit) VALUES ('sucre');
INSERT INTO produit (nom_produit) VALUES ('mdma')
ON DUPLICATE KEY UPDATE nom_produit = VALUES(nom_produit);

INSERT INTO produit_recette (id_produit, id_recette, quantite) VALUES
((SELECT id_produit FROM produit WHERE nom_produit = 'oeuf'), @id_recette, 1),
((SELECT id_produit FROM produit WHERE nom_produit = 'farine'), @id_recette, 100),
((SELECT id_produit FROM produit WHERE nom_produit = 'sucre'), @id_recette, 100),
((SELECT id_produit FROM produit WHERE nom_produit = 'lait'), @id_recette, 100),
((SELECT id_produit FROM produit WHERE nom_produit = 'mdma'), @id_recette, 29);
