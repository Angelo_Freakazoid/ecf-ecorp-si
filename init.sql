DROP DATABASE IF EXISTS resto;
CREATE DATABASE resto;
USE resto;

CREATE TABLE `produit` (
  `id_produit` INT PRIMARY KEY AUTO_INCREMENT UNIQUE,
  `nom_produit` VARCHAR(255) UNIQUE
);

CREATE TABLE `restaurant` (
  `id_restaurant` INT PRIMARY KEY AUTO_INCREMENT,
  `nom_restaurant` VARCHAR(255) UNIQUE
);

CREATE TABLE `stock` (
  `id_stock` INT PRIMARY KEY AUTO_INCREMENT,
  `id_produit` INT,
  `id_restaurant` INT,
  `quantite` INT,
  `date_inventaire` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id_produit`),
  FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`),
  UNIQUE(id_produit, id_restaurant, date_inventaire)
);

DROP user IF EXISTS 'alucard'@'localhost';
create user 'alucard'@'localhost' identified by 'coucou123';
grant all privileges on resto.* to 'alucard'@'localhost';

