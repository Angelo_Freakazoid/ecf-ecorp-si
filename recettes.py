import pandas as pd
from unidecode import unidecode
import mysql.connector


def normalize_text(text):
    text = text.strip()
    text = text.replace('"', '')
    text = unidecode(text)

    first_space_index = text.find(' ')
    if first_space_index != -1:
        text = text[:first_space_index].capitalize() + text[first_space_index:]
    else:
        text = text.capitalize()

    if text.endswith('s') and text.lower() not in ['parfums']:
        text = text[:-1]

    return text


file_path = 'recettes.csv'
data = pd.read_csv(file_path)
data = data.rename(columns={'ingredient': 'id_produit', 'name': 'nom_recette', 'spoon': 'quantite'})
data['nom_recette'] = data['nom_recette'].apply(normalize_text)
data['id_produit'] = data['id_produit'].apply(normalize_text)


print(data.head(10))

mydb = mysql.connector.connect(
    host='localhost',
    user='alucard',
    password='coucou123',
    db='resto'
)

cursor = mydb.cursor()

if 'nom_recette' not in data.columns:
    raise KeyError("'nom_recette' n'est pas une colonne dans le DataFrame. Vérifiez le fichier CSV et les noms de colonnes.")

recettes = data['nom_recette'].unique()
for nom_recette in recettes:
    try:
        cursor.execute("INSERT INTO recette (nom_recette) VALUES (%s) ON DUPLICATE KEY UPDATE nom_recette = VALUES(nom_recette)", (nom_recette,))
        mydb.commit()
    except mysql.connector.Error as err:
        print("Une erreur est survenue lors de l'insertion de la recette:", err)

for index, row in data.iterrows():
    try:
        cursor.execute("SELECT id_produit FROM produit WHERE nom_produit = %s", (row['id_produit'],))
        result = cursor.fetchone()
        cursor.fetchall()

        if not result:
            cursor.execute("INSERT INTO produit (nom_produit) VALUES (%s)", (row['id_produit'],))
            mydb.commit()
            id_produit = cursor.lastrowid
        else:
            id_produit = result[0]

        cursor.execute("""
            INSERT INTO produit_recette (id_produit, id_recette, quantite) 
            SELECT %s, r.id_recette, %s 
            FROM recette r 
            WHERE r.nom_recette = %s
            """, (id_produit, row['quantite'], row['nom_recette']))
        mydb.commit()
    except mysql.connector.Error as err:
        print("Une erreur est survenue lors de l'insertion dans produit_recette:", err)

cursor.close()
mydb.close()