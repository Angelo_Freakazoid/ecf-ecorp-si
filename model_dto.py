from pydantic import BaseModel


class Produit(BaseModel):
  id_produit: int | None = None
  nom_produit: str

class Recette(BaseModel):
  id_recette: int | None = None
  nom_recette: str
  ingredients: list

class StockInfo(BaseModel):
  nom_restaurant: str
  quantite: int

class StockGlobalInfo(BaseModel):
  stock_par_restaurant: list[StockInfo]
  stock_global: int

class StockUpdate(BaseModel):
  id_produit: int
  id_restaurant: int
  quantite_change: int  

class Ingredient(BaseModel):
  nom_produit: str
  quantite: int

class RecetteCreate(BaseModel):
  nom_recette: str
  ingredients: list[Ingredient]

class NombreDeFois(BaseModel):
  nom_restaurant: str
  fois_realisable: int

class RecetteRealisable(BaseModel):
  nom_recette: str
  realisable_par_restaurant: list[NombreDeFois]