import pandas as pd                 # Importe le module pandas et lui attribue l'alias 'pd' pour simplifier les appels ultérieurs.
import mysql.connector              # Importe le module mysql.connector qui permet de se connecter à une base de données MySQL.
from unidecode import unidecode     # Importe la fonction unidecode du module unidecode, utilisée pour normaliser les chaînes de caractères.
from datetime import datetime       # Importe la classe datetime du module datetime, utilisée pour manipuler les dates et les heures.
from pprint import pprint           # Importe la fonction pprint du module pprint, utilisée pour afficher de manière formatée les structures de données.

def normalize_text(text):
    text = unidecode(text)          # Normalise le texte en supprimant les accents et caractères spéciaux.

    first_space_index = text.find(' ')   # Trouve l'index du premier espace dans le texte.
    if first_space_index != -1:          # Vérifie si un espace a été trouvé.
        text = text[:first_space_index].capitalize() + text[first_space_index:]  # Capitalise le premier mot.
    else:
        text = text.capitalize()         # Si aucun espace n'est trouvé, capitalise toute la chaîne.
    if text.endswith('s') and text.lower() not in ['parfums']: # Vérifie si le texte se termine par 's' et n'est pas 'parfums'.
        text = text[:-1]                 # Enlève le 's' final si les conditions sont remplies.
    return text                          # Retourne le texte normalisé.

file_path = 'extract_A.csv'              # Définit le chemin vers le fichier CSV à lire.

data = pd.read_csv(file_path)            # Lit le fichier CSV et le stocke dans un DataFrame pandas.
data['product'] = data['product'].apply(normalize_text)  # Applique la fonction de normalisation au champ 'product'.
data.rename(columns={'product': 'produit', 'stock': 'quantite'}, inplace=True)  # Renomme les colonnes 'product' et 'stock'.
today_date = datetime.now().strftime('%Y-%m-%d')  # Obtient la date actuelle formatée en chaîne de caractères.
data['date'] = today_date               # Ajoute la date actuelle comme nouvelle colonne dans le DataFrame.
summed_data = data.groupby('produit').sum() # Groupe les données par produit et calcule la somme pour chaque groupe.
summed_data['date'] = today_date        # Ajoute la date actuelle à la nouvelle DataFrame agrégée.

# pprint(summed_data)                   

mydb = mysql.connector.connect(         # Crée une instance de connexion (connexion unique) à une base de données MySQL.
    host='localhost',                   # Spécifie l'adresse du serveur de la base de données.
    user='alucard',                     # Définit le nom d'utilisateur pour la connexion.
    password='coucou123',               # Définit le mot de passe pour l'utilisateur.
    db='resto'                          # Spécifie la base de données à utiliser.
)

cursor = mydb.cursor()  # Crée un objet cursor à partir de l'instance de connexion mydb. Ce cursor est utilisé pour exécuter des requêtes SQL.

nom_restaurant = "Restaurant_A"  # Définit le nom du restaurant.
id_restaurant = 1  # Définit l'identifiant du restaurant.

# Exécute une requête SQL pour insérer un nouveau restaurant ou mettre à jour le nom si l'identifiant existe déjà.
cursor.execute("INSERT INTO restaurant (id_restaurant, nom_restaurant) VALUES (%s, %s) ON DUPLICATE KEY UPDATE nom_restaurant = VALUES(nom_restaurant)", (id_restaurant, nom_restaurant))
mydb.commit()  # Valide (commit) la transaction pour s'assurer que les modifications sont enregistrées dans la base de données.

for index, row in data.iterrows():  # Itère sur chaque ligne du DataFrame 'data'.
    try:
        # Sélectionne l'identifiant du produit correspondant au nom du produit dans la ligne actuelle.
        cursor.execute("SELECT id_produit FROM produit WHERE nom_produit = %s", (row['produit'],))
        result = cursor.fetchone()  # Récupère le premier résultat de la requête.
        cursor.fetchall()  # Récupère tous les autres résultats pour nettoyer le cursor.

        if not result:  # Vérifie si aucun produit correspondant n'a été trouvé.
            # Insère un nouveau produit avec le nom du produit actuel s'il n'existe pas déjà.
            cursor.execute("INSERT INTO produit (nom_produit) VALUES (%s)", (row['produit'],))
            mydb.commit()  # Valide la transaction.
            id_produit = cursor.lastrowid  # Récupère l'identifiant du dernier produit inséré.
        else:
            id_produit = result[0]  # Utilise l'identifiant du produit existant.

        # Ce bloc de code est une requête SQL qui est utilisée pour insérer des données dans la table stock 
        # ou pour mettre à jour ces données si une clé unique ou une clé primaire correspondante existe déjà. 
        cursor.execute("""
            INSERT INTO stock (quantite, date_inventaire, id_produit, id_restaurant) 
            VALUES (%s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE 
            quantite = quantite + VALUES(quantite), 
            date_inventaire = VALUES(date_inventaire)
            """, (row['quantite'], row['date'], id_produit, id_restaurant))
        mydb.commit()  # Valide la transaction.
    except mysql.connector.Error as err:
        print("Une erreur est survenue:", err)  # Affiche un message d'erreur si une exception SQL est levée.

cursor.close()  # Ferme l'objet cursor.
mydb.close()  # Ferme la connexion à la base de données.
