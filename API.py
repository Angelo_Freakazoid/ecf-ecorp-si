from typing import List
import mysql.connector
from fastapi import FastAPI, HTTPException
from model_dto import Produit, StockInfo, StockGlobalInfo, StockUpdate, Ingredient, RecetteCreate, Recette, NombreDeFois, RecetteRealisable

# uvicorn API:app --reload

mydb = mysql.connector.connect(
  host="localhost",
  user='alucard',
  password='coucou123', db='resto'
)

cursor = mydb.cursor(dictionary=True)
app = FastAPI()


# Route de test (pas demandé dans l'exo, j'avais envie de tester)

@app.get('/produit', response_model=List[Produit])
def get_product():
    cursor.execute("SELECT * FROM produit")
    produits = cursor.fetchall()
    if not produits:
        raise HTTPException(status_code=404, detail="Produits not found")
    return produits

# En envoyant le nom d’un produit, connaître l’état des stocks dans chaque restaurant ainsi que le stock global (cumulé).
# Merci à Julien et son amis chatGPT, pour leur aide. (Mais j'ai comprit la requete) 

@app.get("/stock/{nom_produit}", response_model=StockGlobalInfo)
def get_stock(nom_produit: str):
    query ="""SELECT 
                        p.nom_produit,
                        r.nom_restaurant,
                        s.quantite 
                    FROM 
                        stock s
                    JOIN 
                        produit p ON s.id_produit = p.id_produit
                    JOIN
                        restaurant r ON s.id_restaurant = r.id_restaurant
                    JOIN
                       (SELECT 
                            s.id_produit, 
                            s.id_restaurant, 
                            MAX(s.date_inventaire) AS derniere_date
                        FROM 
                            stock s
                        JOIN 
                            produit p ON s.id_produit = p.id_produit
                        JOIN
                            restaurant r ON s.id_restaurant = r.id_restaurant
                        WHERE 
                            p.nom_produit = %s
                        GROUP BY
                            s.id_produit, s.id_restaurant) AS dernier_ajout
                    ON 
                        s.id_produit = dernier_ajout.id_produit AND
                        s.id_restaurant = dernier_ajout.id_restaurant AND
                        s.date_inventaire = dernier_ajout.derniere_date
                    WHERE 
                        p.nom_produit = %s;"""

    cursor.execute(query, (nom_produit, nom_produit))    
    result = cursor.fetchall()
    if not result:
        raise HTTPException(status_code=404, detail="Produit non trouvé ou stock vide")

    stock_par_restaurant = [StockInfo(nom_restaurant=row['nom_restaurant'], quantite=row['quantite']) for row in result]
    stock_global = sum(row['quantite'] for row in result)

    return StockGlobalInfo(stock_par_restaurant=stock_par_restaurant, stock_global=stock_global)

# Pouvoir ajouter un nouveau produit.

@app.post("/ajout_produit", response_model=Produit)
def create_product(produit_data: Produit):
    query = "INSERT INTO produit (nom_produit) VALUES (%s)"
    values = (produit_data.nom_produit, )

    try:
        cursor.execute(query, values)
        mydb.commit()
        return produit_data
    except mysql.connector.Error as err:
        mydb.rollback()
        raise HTTPException(status_code=500, detail=f"Erreur de base de données: {err}")

# Pouvoir augmenter ou diminuer le stock d’un produit dans un restaurant donné. (il ne doit pas être negatif !)

@app.post("/stock/update")
def update_stock(stock_update: StockUpdate):
    cursor = mydb.cursor(dictionary=True)
    query = "SELECT quantite FROM stock WHERE id_produit = %s AND id_restaurant = %s"
    cursor.execute(query, (stock_update.id_produit, stock_update.id_restaurant))
    result = cursor.fetchone()

    if result is None:
        if stock_update.quantite_change < 0:
            raise HTTPException(status_code=400, detail="Impossible de réduire un stock inexistant")
        else:
            insert_query = "INSERT INTO stock (id_produit, id_restaurant, quantite) VALUES (%s, %s, %s)"
            cursor.execute(insert_query, (stock_update.id_produit, stock_update.id_restaurant, stock_update.quantite_change))
    else:
        current_stock = result['quantite']
        new_stock = current_stock + stock_update.quantite_change

        if new_stock < 0:
            raise HTTPException(status_code=400, detail="Le stock ne peut pas devenir négatif")

        update_query = "UPDATE stock SET quantite = %s WHERE id_produit = %s AND id_restaurant = %s"
        cursor.execute(update_query, (new_stock, stock_update.id_produit, stock_update.id_restaurant))

    mydb.commit()
    cursor.close()
    return {"message": "Stock mis à jour avec succès"}

# Ajouter une recette (la liste de ses ingrédients et les quantités).

@app.post("/recette")
def create_recette(recette: RecetteCreate):
    try:
        cursor = mydb.cursor(buffered=True)
        
        cursor.execute("SELECT id_recette FROM recette WHERE nom_recette = %s", (recette.nom_recette,))
        existing_recette = cursor.fetchone()

        if existing_recette:
            cursor.execute("DELETE FROM produit_recette WHERE id_recette = %s", (existing_recette[0],))
            cursor.execute("DELETE FROM recette WHERE id_recette = %s", (existing_recette[0],))
            mydb.commit()

        cursor.execute("INSERT INTO recette (nom_recette) VALUES (%s)", (recette.nom_recette,))
        mydb.commit()
        id_recette = cursor.lastrowid

        for ingredient in recette.ingredients:
            cursor.execute("""
                INSERT INTO produit (nom_produit) VALUES (%s)
                ON DUPLICATE KEY UPDATE nom_produit = VALUES(nom_produit)
            """, (ingredient.nom_produit,))
            mydb.commit()

            cursor.execute("SELECT id_produit FROM produit WHERE nom_produit = %s", (ingredient.nom_produit,))
            id_produit = cursor.fetchone()[0]

            cursor.execute("""
                INSERT INTO produit_recette (id_produit, id_recette, quantite) VALUES (%s, %s, %s)
            """, (id_produit, id_recette, ingredient.quantite))
            mydb.commit()

        cursor.close()
        return {"message": "Recette créée ou mise à jour avec succès"}

    except Exception as e:
        cursor.close()
        raise HTTPException(status_code=500, detail=str(e))



# Pouvoir retrouver une recette grâce à son nom.

@app.get("/recette/{nom_recette}", response_model=Recette)
def get_recette(nom_recette: str):
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT id_recette FROM recette WHERE nom_recette = %s", (nom_recette,))
        recette = cursor.fetchone()
        if not recette:
            raise HTTPException(status_code=404, detail="Recette non trouvée")

        cursor.execute("""
            SELECT p.nom_produit, pr.quantite
            FROM produit_recette pr
            JOIN produit p ON pr.id_produit = p.id_produit
            WHERE pr.id_recette = %s
        """, (recette['id_recette'],))

        ingredients = [Ingredient(nom_produit=row['nom_produit'], quantite=row['quantite']) for row in cursor.fetchall()]

    return Recette(nom_recette=nom_recette, ingredients=ingredients)

# Pouvoir savoir combien de fois je peux faire une recette donnée dans chacun des restaurants.

@app.get("/recette/realisable/{nom_recette}", response_model=RecetteRealisable)
def get_recette_realisable(nom_recette: str):
    try:
        cursor = mydb.cursor(dictionary=True, buffered=True)
        cursor.execute("SELECT id_recette FROM recette WHERE nom_recette = %s", (nom_recette,))
        recette = cursor.fetchone()
        if not recette:
            raise HTTPException(status_code=404, detail="Recette non trouvée")
        id_recette = recette['id_recette']

        cursor.execute("""
            SELECT pr.id_produit, pr.quantite
            FROM produit_recette pr
            WHERE pr.id_recette = %s
        """, (id_recette,))
        ingredients = cursor.fetchall()
        realisable = []
        cursor.execute("SELECT id_restaurant, nom_restaurant FROM restaurant")
        restaurants = cursor.fetchall()
        for restaurant in restaurants:
            fois_realisable = 0  

            for ingredient in ingredients:
                cursor.execute("""
                    SELECT quantite FROM stock
                    WHERE id_produit = %s AND id_restaurant = %s
                """, (ingredient['id_produit'], restaurant['id_restaurant']))
                stock = cursor.fetchone()

                if stock:
                    fois = stock['quantite'] // ingredient['quantite']
                    if fois_realisable == 0:
                        fois_realisable = fois
                    else:
                        fois_realisable = min(fois_realisable, fois)
                else:
                    fois_realisable = 0
                    break
            realisable.append(NombreDeFois(nom_restaurant=restaurant['nom_restaurant'], fois_realisable=fois_realisable))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    finally:
        cursor.close()

    return RecetteRealisable(nom_recette=nom_recette, realisable_par_restaurant=realisable)
